package storage;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Student{
    int id;
    String name;
    String gender;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Student(int id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }
    public Student(){

    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
    public class Testing {
    static int currentpage = 0;
    static CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
    public static void main(String[] args){
        //List<Integer> stu = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        Student st = new Student();
        List<Student> arr = new ArrayList<>();
        for (int i=0;i<10;i++){
            arr.add(new Student(i+1,"hello","M"));
        }
        System.out.print("Enter page: ");
        String num = sc.nextLine();
        int intnum = Integer.parseInt(num);

        int cpage = dispage(arr,intnum);
        System.out.println("New Page");
        int spage = dispage(arr,cpage+1);
        System.out.println("New Page");
        dispage(arr,spage+1);
        //hello friends
    }
    public static int dispage(List<Student> arr,int page){
        List<Integer> numk = new ArrayList<>();//store num
        Table t = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.addCell(" ID ",numberStyle);
        t.addCell(" Name ",numberStyle);
        t.addCell(" Gender ",numberStyle);
        int j=0;
        for (int i=-1;i<arr.size();i+=3){
            numk.add(i);
        }
        if (page == 0){
            page =1;
            int k = page + numk.get(page-1);
            for (int i=k;i<arr.size();i++){
                if (j==3){
                    if ((arr.size()%j) == 0){
                        int num = (arr.size()/j);
                        if (page>num){
                            int npage = page - num;
                            t.addCell("Page "+npage+" /"+num ,numberStyle,3);
                        }else {
                            t.addCell("Page "+page+" /"+num ,numberStyle,3);
                        }
                    }else {
                        int num = (arr.size()/j)+1;
                        if (page>num){
                            int npage = page - num;
                            t.addCell("Page "+npage+" /"+num ,numberStyle,3);
                        }else {
                            t.addCell("Page "+page+" /"+num ,numberStyle,3);
                        }
                    }
                    break;
                }else {
                    String id = String.valueOf(arr.get(i).getId());
                    t.addCell(" "+id+" ",numberStyle);
                    t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                    t.addCell(" "+arr.get(i).getGender()+" ",numberStyle);
                    j = j+1;
                }
            }
        }
        else {
            if ((arr.size()%4)==0){
                if ((arr.size()/4)>= page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==4){
                            t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,1);
                            t.addCell("Record: "+arr.size(),numberStyle,2);
                            break;
                        }else {
                            String id = String.valueOf(arr.get(i).getId());
                            t.addCell(" "+id+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getGender()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page ==(arr.size()/4) ){
                    t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,1);
                    t.addCell("Record: "+arr.size(),numberStyle,2);
                }
            }//---------------------------------------------------------------------------------------------------------
            else {
                int nsize = (arr.size()/4)+1;
                if (nsize>=page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==4){
                            t.addCell("Page "+page+" /"+nsize,numberStyle,1);
                            t.addCell("Record: "+arr.size(),numberStyle,2);
                            break;
                        }else {
                            String id = String.valueOf(arr.get(i).getId());
                            t.addCell(" "+id+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getGender()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page == nsize){
                    t.addCell("Page "+page+" /"+nsize,numberStyle,1);
                    t.addCell("Record: "+arr.size(),numberStyle,2);
                }
            }
        }
        System.out.println(t.render());
        return page;
    }



    public static void display(Student st, List<Student> arr) {

        Table t = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        Table t1 = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);

        t.addCell("ID",numberStyle);
        t.addCell("Name",numberStyle);
        t.addCell("Gender",numberStyle);
        t1.addCell("ID",numberStyle);
        t1.addCell("Name",numberStyle);
        t1.addCell("Gender",numberStyle);

        System.out.println("page 1");
        for (int i= currentpage;i< currentpage + 3;i++){
            String id = String.valueOf(arr.get(i).getId());
            t.addCell(id,numberStyle);
            t.addCell(arr.get(i).getName(),numberStyle);
            t.addCell(arr.get(i).getGender(),numberStyle);
        }
        System.out.println(t.render());

        System.out.println("page 2");
        currentpage+=3;
        for (int i =currentpage;i< currentpage+3;i++){
            String id = String.valueOf(arr.get(i).getId());
            t1.addCell(id,numberStyle);
            t1.addCell(arr.get(i).getName(),numberStyle);
            t1.addCell(arr.get(i).getGender(),numberStyle);
        }
        System.out.println(t1.render());

    }
    public void page(List<Student> arr,Student st){
        Table t = new Table(3, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.addCell(" ID ",numberStyle);
        t.addCell(" Name ",numberStyle);
        t.addCell(" Gender ",numberStyle);
        int k = 0;
        int j=0;
        for (int i=0;i<arr.size();i++){
            String id = String.valueOf(arr.get(i).getId());
            t.addCell(" "+id+" ",numberStyle);
            t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
            t.addCell(" "+arr.get(i).getGender()+" ",numberStyle);
            if (k == 2){
                j = j+1;
                t.addCell("Page "+j,numberStyle,3);
                k=0;
            }else {
                k = k+1;
            }
        }
        System.out.println(t.render());
    }
}
