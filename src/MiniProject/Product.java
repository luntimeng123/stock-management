package MiniProject;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.Serializable;
import java.util.*;

public class Product implements Serializable {
    //class member declaration
    private int id;
    private String name;
    private double unit_price;
    private int qty;
    private String date;

    //all getter
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getUnit_price() {
        return unit_price;
    }

    public int getQty() {
        return qty;
    }

    public String getDate() {
        return date;
    }
    //all setter

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit_price(double unit_price) {
        this.unit_price = unit_price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setDate(String date) {
        this.date = date;
    }
    //constructor
    public Product(){

    }
    //constructor for Product
    public Product(int id,String name,double unit_price,int qty,String date){
        this.id = id;
        this.name = name;
        this.unit_price = unit_price;
        this.qty = qty;
        this.date = date;
    }
    //toString method to get all data from Product class
    public String toString(){
        return "id: "+this.id+" "+"name: "+this.name+" "+"unite-price: "+this.unit_price+" "+"QTY: "+this.qty+" "+"Date: "+this.date;
    }
}













