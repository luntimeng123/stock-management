package MiniProject;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import javax.tools.FileObject;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;


public class ManageTools implements Serializable{
    //set table---------------------------------------------------------------------------------------------------------
    public int dispage(List<Product> arr, int page){
        List<Integer> numk = new ArrayList<>();//store num
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.addCell(" ID ",numberStyle);
        t.addCell(" Product-Name ",numberStyle);
        t.addCell(" Uint-Price ",numberStyle);
        t.addCell(" QTY ",numberStyle);
        t.addCell(" Date ",numberStyle);
        int j=0;
        for (int i=-1;i<arr.size();i+=3){
            numk.add(i);
        }
        if (page == 0){
            page =(arr.size()/4);
            if ((arr.size()%4)==0){
                int k = page + numk.get(page-1);
                for (int i=k;i<arr.size();i++){
                    if (j==4){
                        t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,2);
                        t.addCell("Record: "+arr.size(),numberStyle,3);
                        break;
                    }else {
                        t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                        j = j+1;
                    }
                }
                //check last page
                if (page == (arr.size()/4)){
                    t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }else {
                page=page+1;
                int k = page + numk.get(page-1);
                for (int i=k;i<arr.size();i++){
                    if (j==4){
                        t.addCell("Page "+page+" /"+((arr.size()/4)+1),numberStyle,2);
                        t.addCell("Record: "+arr.size(),numberStyle,3);
                        break;
                    }else {
                        t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                        j = j+1;
                    }
                }
                //check last page
                System.out.println(page);
                if (page == ((arr.size()/4)+1)){
                    t.addCell("Page "+page+" /"+((arr.size()/4)+1),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }
        } else {
            if ((arr.size()%4)==0){
                if ((arr.size()/4)>= page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==4){
                            t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,2);
                            t.addCell("Record: "+arr.size(),numberStyle,3);
                            break;
                        }else {
                            t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page ==(arr.size()/4) ){
                    t.addCell("Page "+page+" /"+(arr.size()/4),numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }//---------------------------------------------------------------------------------------------------------
            else {
                int nsize = (arr.size()/4)+1;
                if (nsize>=page){
                    int k = page + numk.get(page-1);
                    for (int i=k;i<arr.size();i++){
                        if (j==4){
                            t.addCell("Page "+page+" /"+nsize,numberStyle,2);
                            t.addCell("Record: "+arr.size(),numberStyle,3);
                            break;
                        }else {
                            t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                            t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                            j = j+1;
                        }
                    }
                }else {
                    System.out.println("out of page range.!");
                }
                //check last page
                if (page == nsize){
                    t.addCell("Page "+page+" /"+nsize,numberStyle,2);
                    t.addCell("Record: "+arr.size(),numberStyle,3);
                }
            }
        }
        System.out.println(t.render());
        return page;
    }

    //write data--------------------------------------------------------------------------------------------------------
    public int writ(List<Product> arr,int autoId){
        Product p = new Product();
        Scanner sc = new Scanner(System.in);

        LocalDateTime datetime1 = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formatDateTime = datetime1.format(format);

        System.out.print(" " + "-->> id: " + autoId + "\n");
        System.out.print(" " + "-->> name: ");
        String strname = sc.nextLine();
        System.out.print(" " + "-->> price: ");
        String strprice = sc.nextLine();
        double price = Double.parseDouble(strprice);
        System.out.print(" " + "-->> qty: ");
        String strqty = sc.nextLine();
        int qty = Integer.parseInt(strqty);
        p = new Product(autoId, strname, price, qty, formatDateTime);

        arr.add(p);

        return arr.get(arr.size()-1).getId();
    }

    //read data by id---------------------------------------------------------------------------------------------------
    public void readId(List<Product> arr,int j){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.addCell("ID ",numberStyle);
        t.addCell("Product-Name ",numberStyle);
        t.addCell("Uint-Price ",numberStyle);
        t.addCell("QTY ",numberStyle);
        t.addCell("Date ",numberStyle);
        for (int i=0;i<arr.size();i++){
            if (arr.get(i).getId() == j){
                t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
            }else {
                continue;
            }
        }
        System.out.println(t.render());
    }

    //update data-------------------------------------------------------------------------------------------------------
    public List<Product> updat(List<Product> arr,int j){
        Validation val = new Validation();
        Scanner sc = new Scanner(System.in);

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        Table t1 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        readId(arr,j);
        j = j-1;
        t.addCell("1)name "+" | ",numberStyle);
        t.addCell("2)price "+" | ",numberStyle);
        t.addCell("3)qty "+" | ",numberStyle);
        t.addCell("4)All "+" | ",numberStyle);
        t.addCell("5)menu "+" | ",numberStyle);
        System.out.println(t.render());

        System.out.print("-->> Choose update option: ");
        String udatoption = sc.nextLine();
        boolean check = val.checkNum(udatoption);
        while (!check){
            System.out.println("Enter only number: ");
            udatoption = sc.nextLine();
            check = val.checkNum(udatoption);
        }

        int updatint = Integer.parseInt(udatoption);
        while (updatint!= 5){
            switch (updatint){
                case 1:
                    LocalDateTime datetime1 = LocalDateTime.now();
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    String formatDateTime = datetime1.format(format);
                    System.out.print("Enter name: ");
                    String name = sc.nextLine();
                    arr.get(j).setName(name);
                    arr.get(j).setDate(formatDateTime);
                    t1.addCell(" Success Update "+name,numberStyle);
                    System.out.println(t1.render());
                    System.out.println(t.render());
                    System.out.print("-->> Choose update option: ");
                    udatoption = sc.nextLine();
                    check = val.checkNum(udatoption);
                    while (!check){
                        System.out.println("Enter only number: ");
                        udatoption = sc.nextLine();
                        check = val.checkNum(udatoption);
                    }
                    updatint = Integer.parseInt(udatoption);
                    break;
                case 2:
                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);
                    System.out.print("Enter price: ");
                    String price = sc.nextLine();
                    double dprice = Double.parseDouble(price);
                    arr.get(j).setUnit_price(dprice);
                    arr.get(j).setDate(formatDateTime);
                    t1.addCell("Success Update "+price,numberStyle);
                    System.out.println(t1.render());
                    System.out.println(t.render());
                    System.out.print("-->> Choose update option: ");
                    udatoption = sc.nextLine();
                    check = val.checkNum(udatoption);
                    while (!check){
                        System.out.println("Enter only number: ");
                        udatoption = sc.nextLine();
                        check = val.checkNum(udatoption);
                    }
                    updatint = Integer.parseInt(udatoption);
                    break;
                case 3:
                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);
                    System.out.print("Enter qty: ");
                    String qty = sc.nextLine();
                    int intqty = Integer.parseInt(qty);
                    arr.get(j).setQty(intqty);
                    arr.get(j).setDate(formatDateTime);
                    t1.addCell("Success Update "+qty,numberStyle);
                    System.out.println(t1.render());
                    System.out.println(t.render());
                    System.out.print("-->> Choose update option: ");
                    udatoption = sc.nextLine();
                    check = val.checkNum(udatoption);
                    while (!check){
                        System.out.println("Enter only number: ");
                        udatoption = sc.nextLine();
                        check = val.checkNum(udatoption);
                    }
                    updatint = Integer.parseInt(udatoption);
                    break;
                case 4:
                    datetime1 = LocalDateTime.now();
                    format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    formatDateTime = datetime1.format(format);
                    System.out.print("Enter name: ");
                    name = sc.nextLine();
                    System.out.print("Enter price: ");
                    price = sc.nextLine();
                    dprice = Double.parseDouble(price);
                    System.out.print("Enter qty: ");
                    qty = sc.nextLine();
                    intqty = Integer.parseInt(qty);
                    arr.get(j).setName(name);
                    arr.get(j).setUnit_price(dprice);
                    arr.get(j).setQty(intqty);
                    arr.get(j).setDate(formatDateTime);
                    t1.addCell("Success Update ",numberStyle);
                    System.out.println(t1.render());
                    System.out.println(t.render());
                    System.out.print("-->> Choose update option: ");
                    udatoption = sc.nextLine();
                    check = val.checkNum(udatoption);
                    while (!check){
                        System.out.println("Enter only number: ");
                        udatoption = sc.nextLine();
                        check = val.checkNum(udatoption);
                    }
                    updatint = Integer.parseInt(udatoption);
                    break;
                case 5:
                    System.out.println("Thanks you.!");
                    updatint = 5;
                    break;
                default:
                    System.out.println("Thanks you.!");
                    updatint = 5;
            }
        }
        return arr;
    }

    //Remove data-------------------------------------------------------------------------------------------------------
    public List<Product> remov(List<Product> arr,int j){
        int k=0;

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t1 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        Table t2 = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        t2.addCell("id "+j+" not include in system.!",numberStyle);

        readId(arr,j);

        for (int i=0;i<arr.size();i++){
            if (arr.get(i).getId() == j){

                arr.remove(arr.get(i));
                t1.addCell("Success Remove "+"Product "+j,numberStyle);
                System.out.println(t1.render());
            }else {
                k = k+1;
                if (k == arr.size()){
                    System.out.println(t2.render());
                }else {
                    continue;
                }
            }
        }

        return arr;
    }

    //insert 10M record-------------------------------------------------------------------------------------------------
    public int _10Mrescord(List<Product> arr,int j){

        for (int i=0;i<arr.size();i++){
            if (j == arr.get(i).getId()){
                j = arr.get(i).getId();
            }else {
                j = j-1;
            }
        }

        Validation val = new Validation();
        Scanner sc = new Scanner(System.in);

        System.out.print(" -->Enter Row: ");
        String strrecord = sc.nextLine();
        boolean check = val.checkNum(strrecord);
        while (!check){
            System.out.print(" -->Enter new Row: ");
            strrecord = sc.nextLine();
            check = val.checkNum(strrecord);
        }
        int record = Integer.parseInt(strrecord);

        for (int i=j;i<record+j;i++){
            LocalDateTime datetime1 = LocalDateTime.now();
            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            String formatDateTime = datetime1.format(format);
            arr.add(new Product(i+1,"Nmae",0,0,formatDateTime));
        }

        return arr.get(arr.size()-1).getId();
    }

    //Menu--------------------------------------------------------------------------------------------------------------
    public void menu() throws IOException {

        int defaultpage = 1;
        int autoId;

        ManageTaxt mtext = new ManageTaxt();
        Validation val = new Validation();
        ManageTools tool = new ManageTools();
        Scanner sc = new Scanner(System.in);
        List<Product> arr = new ArrayList<>();

        mtext.loading(arr);
        if (arr.size() == 0){
            autoId = 0;
        }else {
            autoId = arr.get(arr.size()-1).getId();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(13, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        t.addCell(" di)splay "+" | ",numberStyle);
        t.addCell("w)rite "+" | ",numberStyle);
        t.addCell("r)ead "+" | ",numberStyle);
        t.addCell("s)earch "+" | ",numberStyle);
        t.addCell("u)pdate "+" | ",numberStyle);
        t.addCell("d)elete "+" | ",numberStyle);
        t.addCell("f)irst "+" | ",numberStyle);
        t.addCell("n)ext "+" | ",numberStyle);
        t.addCell("p)revious "+" | ",numberStyle);
        t.addCell("l)ast "+" | ",numberStyle);
        t.addCell("g)oto "+" | ",numberStyle);
        t.addCell("sr)setrow "+" | ",numberStyle);
        t.addCell("e)xit ",numberStyle);
        System.out.println(t.render());

        System.out.print("--> Choose Option: ");
        String option = sc.nextLine().toLowerCase();
        boolean check = val.checkStr(option);
        while (!check){
            System.out.print("--> Enter only chracter: ");
            option = sc.nextLine().toLowerCase();
            check = val.checkStr(option);
        }

        //choice for choose option
        while (option != "e"){
            switch (option){
                case "di":
                    System.out.println("Display-----------------");

                    if (arr.size()==0){
                        System.out.println("no data.!");
                    }else {
                        tool.dispage(arr,defaultpage);
                    }

                    System.out.println(t.render());

                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "w":
                    System.out.println("Write data in-----------------");
                    if(arr.size() == 0){
                        autoId = 0;
                    }else {
                        autoId = arr.get(arr.size()-1).getId();
                    }
                    autoId = autoId+1;
                    //tool.writedataFile(arr);

                    int newid = writ(arr,autoId);
                    autoId = newid;

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "r":
                    System.out.println("Read data form array-----------------");

                    System.out.print("-->>Enter id: ");
                    String id = sc.nextLine();
                    check = val.checkNum(id);
                    while (!check){
                        System.out.print("-->>Enter id again: ");
                        id = sc.nextLine();
                        check = val.checkNum(id);
                    }
                    int intid = Integer.parseInt(id);

                    readId(arr,intid);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "s":
                    System.out.println("View data form array by Name-----------------");
                    System.out.print("-->>Enter Name: ");
                    String name = sc.nextLine();
                    //readName(arr,name);
                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "u":
                    System.out.println("Update data form array-----------------");

                    System.out.print("-->>Enter id: ");
                    id = sc.nextLine();
                    check = val.checkNum(id);
                    while (!check){
                        System.out.print("-->>Enter id again: ");
                        id = sc.nextLine();
                        check = val.checkNum(id);
                    }
                    intid = Integer.parseInt(id);

                    updat(arr,intid);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "d":
                    System.out.println("delete data form array-----------------");

                    System.out.print("-->>Enter id: ");
                    id = sc.nextLine();
                    check = val.checkNum(id);
                    while (!check){
                        System.out.print("-->>Enter id again: ");
                        id = sc.nextLine();
                        check = val.checkNum(id);
                    }
                    intid = Integer.parseInt(id);

                    remov(arr,intid);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "f":
                    defaultpage = 1;
                    defaultpage = tool.dispage(arr,defaultpage);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "p":
                    defaultpage =defaultpage-1;
                    defaultpage = tool.dispage(arr,defaultpage);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "n":
                    defaultpage = defaultpage+1;
                    int size = arr.size()/4;
                    if ((arr.size()%4)== 0){
                        if (defaultpage>size){
                            defaultpage = 1;
                        }
                    }else {
                        if (defaultpage>(size+1)){
                            defaultpage = 1;
                        }
                    }
                    defaultpage = tool.dispage(arr,defaultpage);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "l":
                    size = arr.size()/4;
                    if ((arr.size()%4)== 0){
                        defaultpage = size;
                    }else {
                       defaultpage = size + 1;
                    }
                    defaultpage = tool.dispage(arr,defaultpage);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "g":
                    System.out.println("which page you want to-----------------");

                    System.out.print("Enter Page: ");
                    String enterpage = sc.nextLine();
                    int intpage = Integer.parseInt(enterpage);
                    defaultpage = intpage;

                    tool.dispage(arr,defaultpage);

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "sr":
                    System.out.println("Please insert the amount row-----------------");

                    int newId = tool._10Mrescord(arr,autoId);
                    autoId = newId;

                    System.out.println(t.render());
                    System.out.print("--> Choose Option: ");
                    option = sc.nextLine().toLowerCase();
                    check = val.checkStr(option);
                    while (!check){
                        System.out.print("--> Enter only chracter: ");
                        option = sc.nextLine().toLowerCase();
                        check = val.checkStr(option);
                    }
                    break;
                case "e":
                    option = "e";
                    break;
                default:
                    System.out.println("not include option.!");
                    option = "e";
            }
        }
        System.out.println("Thanks you.!");
    }

    // Write data to File-----------------------------------------------------------------------------------------------
    public void writedataFile(List<Product> arr){
        try (
                // write
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("product.txt"))) {

                objectOutputStream.writeObject(arr);

                objectOutputStream.flush();
        } catch (FileNotFoundException e) {
                System.out.println(e);
        } catch (IOException e) {
                System.out.println(e);
        }
    }

    //Read data from File push to ArrayList-----------------------------------------------------------------------------
    public void readdataFile() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("product.txt"));
        List<Object> object= new ArrayList<>();

        object = (List<Object>) objectInputStream.readObject();

        try{
            object.forEach((i)->{
                System.out.println(i);
            });
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}