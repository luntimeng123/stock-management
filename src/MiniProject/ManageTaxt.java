package MiniProject;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ManageTaxt extends Thread{
        //Logo place
        public void process(){
            try{
                List<String> arr = new ArrayList<>();
                arr.add("  _____  _                           _____           _                 _____ ____  ");
                arr.add(" |  __ \\| |                         |  __ \\         | |               / ____|___ \\ ");
                arr.add(" | |__) | |__  _ __   ___  _ __ ___ | |__) |__ _ __ | |__    ______  | |  __  __) |");
                arr.add(" |  ___/| '_ \\| '_ \\ / _ \\| '_ ` _ \\|  ___/ _ \\ '_ \\| '_ \\  |______| | | |_ ||__ < ");
                arr.add(" | |    | | | | | | | (_) | | | | | | |  |  __/ | | | | | |          | |__| |___) |");
                arr.add(" |_|    |_| |_|_| |_|\\___/|_| |_| |_|_|   \\___|_| |_|_| |_|           \\_____|____/ ");
                arr.add("                                                                                   ");
                for (int i=0;i<arr.size();i++){
                    System.out.println(arr.get(i));
                    Thread.sleep(500);
                }
            }catch(Exception e){
                System.out.println(e);
            }
        }
        //loading....
        public void loading(List<Product> arr){
            String text = " Please Wait";
            String time = " Spent time: ";
            String dot = "......";
            int mili = arr.size();
            try {
                System.out.print(text);
                for (int i=0;i<dot.length();i++){
                    System.out.print(dot.charAt(i));
                    Thread.sleep(mili/6);
                }
                System.out.println("\n"+time+(mili/1000)+" s");
                System.out.println("\n");
            }catch (Exception e){
                System.out.println(e);
            }
        }
        //read data by name
        public void readName(List<Product> arr,String name){
            CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
            Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
            t.addCell("ID ",numberStyle);
            t.addCell("Product-Name ",numberStyle);
            t.addCell("Uint-Price ",numberStyle);
            t.addCell("QTY ",numberStyle);
            t.addCell("Date ",numberStyle);
                for (int i=0;i<arr.size();i++){
                    if (arr.get(i).getName().equals(name)){
                        t.addCell(" "+arr.get(i).getId()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getName()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getUnit_price()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getQty()+" ",numberStyle);
                        t.addCell(" "+arr.get(i).getDate()+" ",numberStyle);
                }else {
                    continue;
                }
            }
            System.out.println(t.render());
        }

        //run method
        @Override
        public void run() {
            process();
        }
//        public static void main(String[] args){
//            LocalDateTime datetime1 = LocalDateTime.now();
//            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
//            String formatDateTime = datetime1.format(format);
//            Scanner sc = new Scanner(System.in);
//            Acsii ac = new Acsii();
//            Product p = new Product();
//            List<Product> arr = new ArrayList<>();
//            arr.add(new Product(1,"apple",10,450,formatDateTime));
//            arr.add(new Product(2,"orange",10,450,formatDateTime));
//            arr.add(new Product(3,"orange",10,450,formatDateTime));
//            System.out.print("Enter Name: ");
//            String name = sc.nextLine();
//            //int id = Integer.parseInt(name);
//            ac.readName(arr,name);
//        }
}
